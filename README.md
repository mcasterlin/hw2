![figure_1.png](https://bitbucket.org/repo/KGEjrd/images/1969530785-figure_1.png)

As a sidenote, the following pseudocode composes the basic idea behind the remaining work left to be done (post the -end progress- marker)

* Use Lucas-Kanade method to compare current and previous frames
* 	Apply Gaussian filter to produce detectible gradient paths
* 	Determine direction of pixel value increase in x and y directions
* 	Determine magnitude of this increase
* 	Assign indicator according to corresponding direction and magnitude
* 	Display animation with newly added indicators.

The last part of the program is merely a demonstration of principle for the frame-by-frame read-in of the remaining files using the harris filter, as the time required for the full range of 100 is rather extensive.