# ---------------------------------------------------- #
# File: tri.py
# ---------------------------------------------------- #
# Author(s): Michael Casterlin (mcasterlin), Nita Soni
# ---------------------------------------------------- #
# Plaftorm:    {Windows | Unix (includes Mac OS)}
# Environment: Python 2.7.x
# Libaries:    numpy 1.9.0
#              matplotlib 1.4.0
#       	   scipy 0.14.0-Py2.7
#              . . .
#       	   sympy 0.7.5
# ---------------------------------------------------- #
# Description:
"""
Current Status: 

Optical Flow and Feature Tracking still in development

Known Issues: 

Currently the triangle generation occassionally produces boundary errors
due to the fact that the slope used to constrain the positive space assignments
is only approximated within the matrix's resolution. If this error occurs, 
try re-running the program.
The animation of the spinning triangle seems to become progressively slower,
it is likely that an initialized blit assignment may help resolve this issue.
Also, the constructed Harris Filter can be observed to run abnormally slow,
which dramatically limits the ability to rapidly obtain coordinate data for 
the velocity tracking operation at present.

Future Plans: 

The tracking function would analyze the sum of each coordinate pair relative
to the sums of the previous sets in order to determine which 3 vertices
(in the case of over-definition) are to be used. They would then be correlated
between frames by proximity.

Pseudo-code:

define function for generating random triangle:
	initialize matrix of zeros at 256x256 resolution
	generate random values for vertex coordinates within desired ranges
	determine which y-coordinate of two bottom vertices is lower
	determine three occuring slopes at triangle edges
	iterate through matrix, assigning a value of 1 between slope equation constraints
	stop when first vertex is reached so that new slope boundary is defined
	continue to fill bounded range until two slopes intersect

define function for spinning and saving this resulting triangle:
	request input for angular velocity
	correlate this to degrees rotated per frame
	save 100 frames where each matrix includes this incremental rotation
	animate these frames to display visible spin

define function which applies Morphological Binary Operator filter to find vertices:
	Apply an opening operator to matrix
	subtract the original matrix from this opened matrix
	label all resulting independent structures
	find centers of mass for each structure
	report number and location of all structures

define function which applies Harris filter to find vertices:
	apply sobel operator to matrix in order to find x and y partial derivatives
	form a gaussian weighted window with which to iterate over image matrix
	initialize matrix for results to be saved to with zeros
	progress along columns > rows of matrix (buffered by the window range), where:
		structure tensor is formed from combination of:
			Ix*Ix, Iy*Iy, Iy*Ix
		determinant is then calculated and multiplied by square trace and constant
		if resulting value is above 0.1, mask pixel with 1. Otherwise, 0.
	repeat process of labelling structures, finding centers of mass, and reporting

define function to plot the comparative results of these operations:
	the binary result is plotted to the left,
	the original in the middle,
	and the harris result to the right.

Triangle generation function is called
Spin and Save function is called
data is read from first saved frame, scaled down by 255 for binary comparison
Binary Filter function is called
coordinate memory is initialized
Harris filter function is called; first coordinate set documented
Plotting function is called

For all frames:
	Save information about vertex coordinates as found by harris filter
	Compare these results to previous results in order to reduce error and localize
	(Would then be plotted vs. Time)

	-end progress-

"""
# also using glob, random and math
# ---------------------------------------------------- #

import numpy as np 
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import scipy.ndimage as ndimage
import scipy.misc as misc
from glob import glob
import random
import math

def TriGen():

	# Generating Triangle

	im = np.zeros((256,256))

	mid = int((len(im[0]))/2)
	x = 0

	ay = random.randrange(0,128)
	bx = random.randrange(128,206)
	by = random.randrange(128,256)
	cx = random.randrange(50,128)
	cy = random.randrange(128,256)

	if by < cy:
		low = by
		high = cy
	else:
		low = cy
		high = by

	s1 = (128. - cx)/(low - ay)
	s2 = (bx - 128.)/(low - ay)
	s3 = float(bx - cx) / float(high-low)

	for i in range(0,low):
		x1 = (s1*(i-ay))
		x2 = (s2*(i-ay))
		for j in range(int(mid-x1),int(mid+x2)):
			im[i,j] = 1

	i = low

	left = 0
	right = 1

	while left < right:
		if by < cy:
			x1 = (s1*(i-ay))
			x2 = (-s3*(i-by)+float(bx))
			for j in range(int(mid-x1),int(x2)):
				im[i,j] = 1
			left = mid-x1
			right = x2
		else:
			x1 = (s3*(i-cy)+float(cx))
			x2 = (s2*(i-ay))
			for j in range(int(x1),int(mid+x2)):
				im[i,j] = 1
			left = x1
			right = mid+x2
		i += 1

		return im

def TriSpin(image):

	im = image

	angVel = float(raw_input("\nEnter an angular velocity for the triangle (Standard = 1): "))

	delAng = angVel * 3.6

	print "\nGenerating Triangle..."

	# Rotating and Saving Frames

	for i in range(100):
		misc.imsave('triangle%02d.png' % (i), im)
		im = ndimage.interpolation.rotate(im, angle = delAng, reshape = False)

	# Forming Animation

	fig, ax = plt.subplots()

	def animate(i):
		triangle = ndimage.imread('triangle%02d.png' % i)
		return plt.imshow(triangle, interpolation = 'none')

	ani = animation.FuncAnimation(fig, animate, frames = 100, interval= 25, repeat = True)
	plt.show()

def TriGet():

	for frame in glob('triangle??.png'):
		pass

def BinaryFilter(data):

	# Morphological Binary Filter

	print "\nCalculating Binary Vertex Results..."

	data2 = ndimage.binary_opening(data, structure=np.ones((5,5)).astype(np.int))
	data3 = data2 - data

	s = s = [[1,1,1],[1,1,1],[1,1,1]]
	labeled_array, num_features = ndimage.measurements.label(data3, structure = s)
	print "\nVertices detected: ", num_features
	print "\n",

	num = 1
	for i in ndimage.measurements.center_of_mass(data3, labeled_array, range(1,num_features+1)):
		print "Vertex %d: " % num, i
		num += 1

	return data3

def Harris(data):

	# Harris Filter

	print "\nCalculating Harris-Filter Vertex Results..."

	im2 = data.astype('f')
	im2 = ndimage.filters.gaussian_filter(im2,1)
	Sx = ndimage.sobel(im2, axis = 0, mode = 'constant')
	Sy = ndimage.sobel(im2, axis = 1, mode = 'constant')

	gw = np.zeros((3,3), dtype = 'f')
	center = (3-1)/2
	gw[center,center] = 1
	gw = ndimage.filters.gaussian_filter(gw, 1)

	saveHarris = np.zeros((256,256))

	row = 0
	col = 0

	for i in range(256):
		for j in range(256):
			if row > 1 and row < 254 and col < 253 and col > 0:
				Ix = Sx[row-1:row+2, col-1:col+2]
				Iy = Sy[row-1:row+2, col-1:col+2]

				Ix2 = np.multiply(Ix,Ix)
				Iy2 = np.multiply(Iy,Iy)
				IxIy = np.multiply(Ix,Iy)

				M1 = np.sum(np.multiply(Ix2, gw))
				M2 = np.sum(np.multiply(IxIy, gw))
				M3 = M2
				M4 = np.sum(np.multiply(Iy2, gw))
				M = np.array([[M1, M2],[M3, M4]])

				H = np.linalg.det(M)-0.05*(M1+M4)**2

				saveHarris[row,col] = H
			col += 1
			if col == 256:
				col = 0
				row+=1

	mask = saveHarris > 0.1
	mask.astype(np.int)
	saveHarris = np.multiply(saveHarris, mask)

	s2 = s2 = [[1,1,1],[1,1,1],[1,1,1]]
	labeled_array2, num_features2 = ndimage.measurements.label(saveHarris, structure = s2)
	print "\nVertices detected: ", num_features2
	print "\n",

	register = []
	num2 = 1
	for i in ndimage.measurements.center_of_mass(saveHarris, labeled_array2, range(1,num_features2+1)):
		print "Vertex %d: " % num2, i
		register.append(i)
		num2 += 1

	return saveHarris, list(register)

def TriPlot(plot1, plot2, plot3):

	# Plotting Results

	binPlot = plot1
	genPlot = plot2
	harPlot = plot3

	fig = plt.figure()
	plt.subplot(131)
	plt.title("Binary Filter Vertices\n")
	plt.imshow(binPlot, interpolation = 'none', cmap = plt.cm.gray)
	plt.subplot(132)
	plt.title("Generated Triangle\n")
	plt.imshow(genPlot, interpolation = 'none')
	plt.subplot(133)
	plt.title("Harris Filter Vertices\n")
	plt.imshow(harPlot, interpolation = 'none', cmap = plt.cm.gray)
	plt.show()


im = TriGen()
TriSpin(im)
data = ndimage.imread('triangle00.png')
data = data/255
saveBin = BinaryFilter(data)
coords = [0]
saveHarris, coords[0] = Harris(data)
TriPlot(saveBin, data, saveHarris)


# Storing Optical Flow coordinates

print "\nNow generating the x,y coordinates for each vertex across\n\
all rotation frames for use in plotting feature tracking\n\
(This takes a VERY long time)"

for i in range(1,3):
	print "\nReading frame %02d..." % i
	data = ndimage.imread('triangle%02d.png' % i)
	data = data/125
	tempHarris, newCoord = Harris(data)
	coords.append(newCoord)

# pos = 1
# for i in coords:
# 	lump1 = sum(coords[pos-1][0])
# 	lump2 = sum(coords[pos-1][1])
# 	lump3 = sum(coords[pos-1][2])
# 	print lump1, lump2, lump3
# 	pos+=1